import React from 'react'
import './hamburger.scss'

export function Hamburger({ open, handleMenu }) {
	return (
		<div
			className={`hamburger ${open ? 'open' : ''}`}
			onClick={() => handleMenu(!open)}
		>
			<div />
			<div />
			<div />
		</div>
	)
}
