import React from 'react'
import Ava from 'assets/images/avatar.jpg'
import Dropdown from 'muicss/lib/react/dropdown'
import DropdownItem from 'muicss/lib/react/dropdown-item'
import { options } from 'components/utils/common'
import './avatar.scss'

export function Avatar() {
	return (
		<div className='avatar-wrapper d-flex align-items-center'>
			<div className='username'>
				<Dropdown size='small' label='John Doe' variant='flat'>
					{options.map((item) => {
						return <DropdownItem>{item}</DropdownItem>
					})}
				</Dropdown>
			</div>
			<div className='photo'>
				<img src={Ava} alt='user avatar' />
			</div>
		</div>
	)
}
