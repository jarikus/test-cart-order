import React from 'react'
import Option from 'muicss/lib/react/option'
import MuiSelect from 'muicss/lib/react/select'
import './select.scss'

export function Select({ label, name, options = [], ...rest }) {
	return (
		<div className='select-wrapper'>
			<MuiSelect name={name} placeholder={label} {...rest}>
				{options.map(({ value, label }, index) => (
					<Option key={index} value={value} label={label} />
				))}
			</MuiSelect>
		</div>
	)
}
