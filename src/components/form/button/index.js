import React from 'react'
import ButtonMui from 'muicss/lib/react/button'
import './button.scss'

export function Button({ size, children, color }) {
	const className = `button-wrapper ${color}`
	return (
		<div className={className}>
			<ButtonMui size={size} color={color} variant='flat'>
				{children}
			</ButtonMui>
		</div>
	)
}
