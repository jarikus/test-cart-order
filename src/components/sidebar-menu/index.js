import React from 'react'
import { Icon } from 'components/icons'
import './sidebar-menu.scss'

const menu = [
	{
		id: 1,
		name: 'Dashboard',
		icon: 'Dashboard',
	},
	{
		id: 2,
		name: 'Inventory',
		icon: 'Products',
	},
	{
		id: 3,
		name: 'Open Order',
		icon: 'Invoices',
	},
	{
		id: 4,
		name: 'Order History',
		icon: 'Calendar',
	},
]

export function SideBarMenu() {
	const renderMenu = menu.map(({ id, name, icon }) => (
		<li key={id} className='item d-flex'>
			<Icon component={icon} />
			<span>{name}</span>
		</li>
	))
	return <ul className='sidebar-menu'>{renderMenu}</ul>
}
