import React from 'react'
import './sidebar.scss'
import { Logo } from 'components/logo'
import { SideBarMenu } from 'components/sidebar-menu'

export function SideBar({ open }) {
	return (
		<sidebar className={`sidebar-wrapper ${open ? 'open' : ''}`}>
			<Logo />
			<SideBarMenu />
		</sidebar>
	)
}
