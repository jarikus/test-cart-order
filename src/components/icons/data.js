import React from 'react'
import Notification from '../notification/index'

export const Calendar = () => (
	<svg
		xmlns='http://www.w3.org/2000/svg'
		width='16'
		height='19.767'
		viewBox='0 0 16 19.767'
	>
		<rect width='15' height='15' transform='translate(0 0.026)' fill='none' />
		<path
			d='M2,6.177V17.3H14V6.177ZM13,2.471h2c.6,0,1,.494,1,1.235V18.532c0,.741-.4,1.235-1,1.235H1c-.6,0-1-.494-1-1.235V3.706c0-.741.4-1.235,1-1.235H3V1.235C3,.494,3.4,0,4,0S5,.494,5,1.235V2.471h6V1.235C11,.494,11.4,0,12,0s1,.494,1,1.235ZM12,14.826H10V12.355h2Zm-3,0H7V12.355H9Zm3-3.706H10V8.648h2Zm-3,0H7V8.648H9ZM6,14.826H4V12.355H6Z'
			fill='#768c9a'
			fill-rule='evenodd'
		/>
	</svg>
)

export const Dashboard = () => (
	<svg
		xmlns='http://www.w3.org/2000/svg'
		width='16'
		height='19'
		viewBox='0 0 16 19'
	>
		<path
			id='icon_dashboard'
			d='M13,19c-.6,0-1-.475-1-1.187V7.125c0-.713.4-1.187,1-1.187h2c.6,0,1,.475,1,1.187V17.813C16,18.525,15.6,19,15,19ZM7,19c-.6,0-1-.475-1-1.187V1.187C6,.476,6.4,0,7,0H9c.6,0,1,.476,1,1.187V17.813C10,18.525,9.6,19,9,19ZM1,19c-.6,0-1-.475-1-1.187V13.063c0-.713.4-1.188,1-1.188H3c.6,0,1,.476,1,1.188v4.749C4,18.525,3.6,19,3,19Z'
			fill='#768c9a'
		/>
	</svg>
)

export const Invoices = () => (
	<svg
		xmlns='http://www.w3.org/2000/svg'
		width='14'
		height='19.767'
		viewBox='0 0 14 19.767'
	>
		<path
			d='M14,0H2C1.4,0,1,.494,1,1.235V19.767L4,17.3l2,2.471L8,17.3l2,2.471L12,17.3l3,2.471V1.235C15,.494,14.6,0,14,0ZM12,12.355H4V9.884h8Zm0-4.942H4V4.942h8Z'
			transform='translate(-1)'
			fill='#768c9a'
		/>
	</svg>
)

export const Products = () => (
	<svg
		xmlns='http://www.w3.org/2000/svg'
		width='16'
		height='19'
		viewBox='0 0 16 19'
	>
		<path
			d='M14,19V0h2V19ZM0,19V0H2V19Zm11-5.937V0h2V13.063Zm-5,0V0h4V13.063Zm-3,0V0H5V13.063Z'
			fill='#768c9a'
		/>
	</svg>
)

export const NotificationIcon = () => (
	<svg
		xmlns='http://www.w3.org/2000/svg'
		width='21'
		height='23'
		viewBox='0 0 21 23'
	>
		<g transform='translate(-1395 -20)'>
			<path
				d='M-1371,81h4a2.006,2.006,0,0,1-2,2A2.006,2.006,0,0,1-1371,81Zm-5-1a.945.945,0,0,1-1-1,.945.945,0,0,1,1-1h.5a4.354,4.354,0,0,0,1.5-3V72a4.952,4.952,0,0,1,5-5,4.951,4.951,0,0,1,5,5v3a4.351,4.351,0,0,0,1.5,3h.5a.945.945,0,0,1,1,1,.945.945,0,0,1-1,1Z'
				transform='translate(2772 -40)'
				fill='#bcbcbc'
			/>
			<g
				transform='translate(1406 22)'
				fill='#65a424'
				stroke='#fff'
				strokeWidth='2'
			>
				<circle cx='4' cy='4' r='4' stroke='none' />
				<circle cx='4' cy='4' r='5' fill='none' />
			</g>
		</g>
	</svg>
)

export const SmallDown = () => (
	<svg
		xmlns='http://www.w3.org/2000/svg'
		width='11'
		height='7'
		viewBox='0 0 11 7'
	>
		<path
			d='M8.1,11.6,2.6,6.041,4.026,4.6,8.1,8.718,12.174,4.6,13.6,6.041Z'
			transform='translate(-2.6 -4.6)'
			fill='#bcbcbc'
		/>
	</svg>
)
