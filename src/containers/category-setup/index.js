import React from 'react'
import Form from 'muicss/lib/react/form'
import Container from 'muicss/lib/react/container'
import Col from 'muicss/lib/react/col'
import Row from 'muicss/lib/react/row'
import { Select, Button, Input } from 'components/form'
import { Title } from 'components/typography'
import { options } from 'components/utils/common'
import { useForm, Controller } from 'react-hook-form'
import 'style/index.scss'

export default () => {
	const { control, handleSubmit } = useForm()

	const onSubmit = (data) => console.log(data)

	const renderOptions = (data) =>
		data.map((item) => {
			return {
				value: item,
				label: item,
			}
		})

	return (
		<div className='category-setup-wrapper'>
			<Title className='mui--text-display1'>Product Category Setup</Title>
			<Form class='form' onSubmit={handleSubmit(onSubmit)}>
				<Container>
					<Row>
						<Col xs='12' md='6'>
							<Controller
								as={Select}
								name='category'
								label='Category'
								options={renderOptions(options)}
								control={control}
							/>
						</Col>
						<Col xs='12' md='6'>
							<Controller
								as={Select}
								name='sub-category'
								label='Sub category'
								options={renderOptions(options)}
								control={control}
							/>
						</Col>
						<Col xs='12' md='6'>
							<Controller
								as={Input}
								name='newCategory'
								control={control}
								label='New Category'
							/>
						</Col>
						<Col xs='12' md-offset='6' />
						<Col xs='12' md='6'>
							<Controller
								as={Input}
								label='New Product'
								control={control}
								name='newProduct'
							/>
						</Col>
						<Col xs='12' md='6'>
							<div className='field-button'>
								<Button color='green'>Save</Button>
							</div>
						</Col>
					</Row>
				</Container>
			</Form>
		</div>
	)
}
