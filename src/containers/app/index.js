import React from 'react'
import Layout from 'components/layout'
import CategorySetup from 'containers/category-setup'
import 'style/index.scss'

export default () => {
	return (
		<Layout>
			<CategorySetup />
		</Layout>
	)
}
